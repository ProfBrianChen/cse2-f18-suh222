//CSE2-SURUI-INSERTIONLAB10
import java.util.*;

public class InsertionSortLab10 {
    public static void main(String[] args) {
        int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
        int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        int iterBest = insertionSort(myArrayBest);
        System.out.println();

        int iterWorst = insertionSort(myArrayWorst);
        System.out.println("The total number of operations performed on the sorted array: " + iterBest);
        System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);

    } //end of the main method

    /**
     * sorting
     */
    public static int insertionSort(int[] list) {
        System.out.println(Arrays.toString(list));

        int iterations = 0;

        for (int i = 1; i < list.length; i++) {
            iterations++;

            for (int j = i; j > 0; j--) {
                if (list[j] < list[j - 1]) {
                    iterations++;
                    int temp = list[j];
                    list[j] = list[j - 1];
                    list[j - 1] = temp;
                }
                else {
                    break;
                }
            }

            System.out.println(Arrays.toString(list));
        }
        return iterations;
    }
}
