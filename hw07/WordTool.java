//CSE2-SURUI-HW07
import java.util.*;
public class WordTool {
    public static void main(String[] args) {
        //decalre variables
        char option = 'a';
        String targetWord = "";
        int instance = 0;

        String sampleText = sampleText("");
        System.out.println(sampleText); //call sampleText method
        System.out.println();

        printMenu(option, sampleText, targetWord);

    }//end of main method

    /**sample text method*/
    public static String sampleText(String sampleText) {
        Scanner scnr = new Scanner(System.in);
        String userInput = "";
        int i = 0;//for the loop
        System.out.println("Enter a sample text: ");
        //check input
        while ( i == 0) {
            if (scnr.hasNextLine()) {
                userInput = scnr.nextLine();
                i = 1;
            } else {
                System.out.println("Invalid input.");
                scnr.next();
            }
        }
        System.out.print("You entered: ");
        return sampleText = userInput;
    }//end of the method

    /**printMenu method*/
    public static Character printMenu(char option, String sampleText, String targetWord) {
        //menu list
        System.out.println("MENU");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit\n");
        //prompt user input
        System.out.println("Choose an option:");
        Scanner scnr = new Scanner(System.in);
        int i = 0;
        while (i == 0){
            String input = scnr.next();
            option = input.charAt(0);
            int j = input.length();
            if ( j == 1 ) {
                if (option == 'c') {
                    getNumOfNonWSCharacters(sampleText);
                    i = 1;
                    break;
                } else if (option == 'w') {
                    getNumOfWords(sampleText);
                    i = 1;
                } else if (option == 'f') {
                    findText(sampleText, targetWord);
                    i = 1;
                } else if (option == 'r') {
                    replaceExclamation(sampleText);
                    System.out.println("Edited text: " + replaceExclamation(sampleText));
                    i = 1;
                } else if (option == 's') {
                    shortenSpace(sampleText);
                    System.out.println("Edited text: " + shortenSpace(sampleText));
                    i = 1;
                } else if (option == 'q') {
                    i = 1;
                } else{
                    System.out.println("Invalid input, try agian.");
                    System.out.println("MENU");
                    System.out.println("c - Number of non-whitespace characters");
                    System.out.println("w - Number of words");
                    System.out.println("f - Find text");
                    System.out.println("r - Replace all !'s");
                    System.out.println("s - Shorten spaces");
                    System.out.println("q - Quit\n");
                    System.out.println("Choose an option:");
                }
            } else {
                System.out.println("Invalid input, try agian.");
                System.out.println("MENU");
                System.out.println("c - Number of non-whitespace characters");
                System.out.println("w - Number of words");
                System.out.println("f - Find text");
                System.out.println("r - Replace all !'s");
                System.out.println("s - Shorten spaces");
                System.out.println("q - Quit\n");
                System.out.println("Choose an option:");
            }
        }
        return option;
    }//end of method

    /**getNumOfNonWSCharacters() method.*/
    public static int getNumOfNonWSCharacters(String sampleText){
        int numOfWhitespace = 0;
        int numOfLetter = 0;
        for (int i = 0; i < sampleText.length(); i++){
            if (Character.isWhitespace(sampleText.charAt(i))) {
                numOfWhitespace++;
            }
        }
        int nonWhitespaceChar = sampleText.length() - numOfWhitespace;
        System.out.println("Number of non-whitespace characters: " + nonWhitespaceChar);
        return nonWhitespaceChar;
    }//end of method

    /**getNumOfWords() method*/
    public static int getNumOfWords(String sampleText) {
        int numOfWords = 0;
        sampleText = shortenSpace(sampleText);
        for (int i = 0; i < sampleText.length(); i++){
            if (Character.isWhitespace(sampleText.charAt(i))) {
                numOfWords++;
            }
        }
        ++numOfWords;
        System.out.println("Number of words: " + numOfWords);
        return numOfWords;
    }//end of method

    /**findText() method*/
    public static int findText(String sampleText, String targetWord) {
        System.out.println("Enter a word or phrase to be found:");
        Scanner scnr = new Scanner(System.in);
        targetWord = scnr.next();
        int instance = 0;
        int i = 0;
        int j = 0;
        int temp = 0;
        for (i = 0; i < sampleText.length(); i++) {
            temp = 0;
            for (j = 0; j < targetWord.length(); j++) {
                if (targetWord.charAt(j) == sampleText.charAt(i)) {
                    i++;
                    temp++;
                }
                if (temp == targetWord.length()) {
                    instance++;
                }
            }
        }
        System.out.println("\"" + targetWord + "\" instance: " + instance);
        return instance;
    }//end of method

    /**replaceExclamation()*/
    public static String replaceExclamation(String sampleText) {
        String newText = sampleText.replace('!', '.');
        return newText;
    }//end of method
    /**shortenSpace() method*/
    public static String shortenSpace(String sampleText) {
        String newText = sampleText;
        while (newText.indexOf("  ") != -1) {
            newText = newText.replace("  ", " ");
        }
        return newText;
    }//end of method
}
