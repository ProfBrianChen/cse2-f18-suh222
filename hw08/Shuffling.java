/**
 * CSE2-SURUI-HW8
 *  shuffle(list), getHand(list,index, numCards), and printArray(list)
 * print out all the cards in the deck X
 * shuffle the whole deck of cards X
 * print out the cards in the deck, all shuffled X
 * getting a hand of cards and print them out x
 */

import java.util.*;

public class Shuffling {
    public static void main(String args[]) {
        Scanner scnr = new Scanner(System.in);
        //declaration
        int a = 0; //control the user input loop
        int numCards = 5;
        int index = 51;
        //club, heart, spade, diamond
        String[] suitNames = {"C", "H", "S", "D"};
        String[] rankNames = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
        String[] cards = new String[52];
        String[] hand = new String[5];
        for (int i = 0; i < 52; i++) {
            cards[i] = rankNames[i%13] + suitNames[i/13];
        } //end of creating list of cards
        System.out.println();
        printArray(cards);
        shuffle(cards);
        printArray(cards);
        //ask input from user
        while (a == 0) {
            if (index < numCards) {
                index = 51;
                shuffle(cards);
                hand = getHand(cards, index, numCards);
                printArray(hand);
            } else {
                hand = getHand(cards, index, numCards);
                printArray(hand);
                index = index - numCards;
            }
            System.out.println("Enter a 0 if you want another hand drawn");
            a = scnr.nextInt();
        }
    } //end of the main method

    /**
     *  takes an array of Strings and prints out each element
     *  separated by a space
     */
    public static void printArray(String[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i] + " ");
        }
        System.out.println();
    } // end of the method

    /**
     * shuffle the whole deck of cards
     */
    public static void shuffle(String[] list) {
        System.out.println("Shuffled");
            for (int y = 0; y < list.length; y++) {
                int j = (int) (Math.random() * (list.length));
                //swap list[i] with list[j]
                String temp = list[y];
                list[y] = list[j];
                list[j] = temp;
            }
    } // end of the method

    /**
     * returns an array that holds the number of cards specified in numCards
     *  be taken off at the end of the list of cards
     */
    public static String[] getHand(String[] list, int index, int numCards) {
        System.out.println("Hand");
        String temp[] = new String [5];
        for (int i = 0; i < 5; i++) {
            temp[i] = list [index - i];
        }
        return temp;
    } //end of the method
}