//CSE2-LAB03-Surui;
//obtain the original total; percentage tip; split check; individual payment;
import java.util.Scanner; //import class Scanner;
public class Check{
  public static void main(String[] args){
    Scanner myScanner = new Scanner ( System.in ); //declare an instance to accept input;
    System.out.print("Enter the original cost of the check in the form of xx.xx: ");//for the input;
    double checkCost = myScanner.nextDouble();// method;
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form of xx): ");//input for the tip;
    double tipPercent = myScanner.nextDouble();
    tipPercent /=100; //convert the percentage into a decimal value
      System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    //print out the output
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;//whole dollar amount of cost w
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping the decimal fraction
    dollars = (int)costPerPerson;
    //get dimes amount
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  }
}
