// CSE2-HW03-PYRAMID-SURUI
//calculate the volume
import java.util.Scanner;
public class Pyramid{
  public static void main(String[] arg){
    Scanner myScanner = new Scanner ( System.in );
    System.out.print("The square side of the pyramid is (input length): ");
    double length = myScanner.nextDouble(); //input square length
    System.out.print("The height of the pyramid is (input height): ");
    double height = myScanner.nextDouble(); //input of height 
    //////CALCULATION
    double volume = Math.pow(length,2) * height / 3;
    System.out.println("The volume inside the pyramid is: " + (int)volume + '.');
  }
}