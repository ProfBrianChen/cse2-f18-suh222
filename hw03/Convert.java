//CSE2-Hw03-Surui Huang - Convert
//acres of land; inches of rain; convert to cubic miles
import java.util.Scanner;//import class
public class Convert{
  public static void main(String[] arg){
    Scanner myScanner = new Scanner ( System.in );//declare an instant to accept input
    System.out.print("Enter the affected area in acres: ");// input of land 
    double acresOfLand = myScanner.nextDouble();//method 
    System.out.print("Enter the rainfall in the affected area: ");// input of water
    double inchesOfRain = myScanner.nextInt();
    double acreinchesOfRain = acresOfLand * inchesOfRain;
    double gallonsOfRain = acreinchesOfRain * 27154; 
    double cubicmilesOfRain = gallonsOfRain / 1101117130711.3;
    System.out.println ("The quantity of rainfall is " + cubicmilesOfRain + " cubic miles");//print out results
  }
}