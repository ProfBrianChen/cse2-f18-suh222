//CSE002 - SURUI HUANG- LAB04
//select a number from 1 to 52
// DIAMOND: 1-13, CLUB: 14-26, HEARTS: 27-39, SPADES: 40-52;
//Import class
import java.util.*;
public class CardGenerator{
  public static void main(String[] args){
    int randomNumber = (int)(Math.random()*51+1);
    String suitOfCard;
    //name of the suit
    if ( randomNumber<14 ){
      suitOfCard = "Diamond";
    }
    else if ( randomNumber>13 & randomNumber<27 ){
      suitOfCard = "Club";
    }
    else if ( randomNumber>26 & randomNumber<40){
      suitOfCard = "Heart";
    }
    else
      suitOfCard = "Spade";{
      
    }
    //identify the cards
    String identityOfCards ="";
     
    switch( randomNumber%13 ){
      case 1:
        identityOfCards = "Ace";
        break;
      case 11:
        identityOfCards = "Jack";
        break;
      case 12:
        identityOfCards = "Queen";
        break;
      case 0:
        identityOfCards = "King";
        break;
      default:
        identityOfCards+= randomNumber%13;
        break;
     }
    //print out the combination
    System.out.println( "You picked up the " + identityOfCards + " of " + suitOfCard );
  }
}