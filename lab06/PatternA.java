//cse2-surui-lab6-patternA
/*Check that the inputs are between 1 and 10
PatternA
1      	        	
1 2
1 2 3
1 2 3 4
1 2 3 4 5
1 2 3 4 5 6 */
import java.util.*;
public class PatternA{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    //ask input from the user
    System.out.println("Enter an integer between 1 to 10: ");
    int checkIn = 0;
    int userInput = 0;
    while ( checkIn==0 ){
      if ( scan.hasNextInt() ){
         userInput = scan.nextInt();
        if( (userInput < 11) & (userInput>0) ){
          checkIn = 1;
        }
        else{
          System.out.println("Number need to be 1 to 10: ");
        }
      }
      else{
        System.out.println("Invalid enter, try again: ");
        scan.next();
      }
    }//end of the checking
    int i = 0;
    int j = 1;
    for ( i=0; i<userInput; ++i ){
      for ( j=1; j<=i+1; ++j ){
        System.out.print( j );
      }
      j = 1;
      System.out.println("");
    }
  }
}