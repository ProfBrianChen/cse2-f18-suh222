//cse2-surui-lab6-patternD
import java.util.*;
public class PatternD{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        //ask input from the user
        System.out.println("Enter an integer between 1 to 10: ");
        int checkIn = 0;
        int userInput = 0;
        while ( checkIn==0 ){
            if ( scan.hasNextInt() ){
                userInput = scan.nextInt();
                if( (userInput < 11) & (userInput>0) ){
                    checkIn = 1;
                }
                else{
                    System.out.println("Number need to be 1 to 10: ");
                }
            }
            else{
                System.out.println("Invalid enter, try again: ");
                scan.next();
            }
        }//end of the checking
        for (int i = 0; i < userInput; i++){
            for (int j = userInput-i; j > 0; j-- ){
                System.out.print(j );
            }
            System.out.println("");
        }
    }
}