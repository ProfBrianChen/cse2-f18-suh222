//cse2-lab06-surui-patternb
/*
PatternB
1 2 3 4 5 6
1 2 3 4 5
1 2 3 4
1 2 3
1 2
1
*/
import java.util.*;
public class PatternB{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    //ask input from the user
    System.out.println("Enter an integer between 1 to 10: ");
    int checkIn = 0;
    int userInput = 0;
    while ( checkIn==0 ){
      if ( scan.hasNextInt() ){
         userInput = scan.nextInt();
        if( (userInput < 11) & (userInput>0) ){
          checkIn = 1;
        }
        else{
          System.out.println("Number need to be 1 to 10: ");
        }
      }
      else{
        System.out.println("Invalid enter, try again: ");
        scan.next();
      }
    }//end of the checking
    int i = 1;
    int j = 1;
    for( i=userInput; i>=j; --i ){
      for( j=1; j<=i; ++j ){
        System.out.print(j);
      }
      j = 1;
      System.out.println("");
    }
  }
}