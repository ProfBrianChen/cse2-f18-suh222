//CSE2-SURUI-HW04-Switch-only
import java.util.*;
public class CrapsSwitch{
  public static void main(String [] args){
    Scanner input = new Scanner( System.in );
    //generate random number 
    int randomNumber1 = (int)( Math.random() * 6 + 1 );
    int randomNumber2 = (int)( Math.random() * 6 + 1 );
    String identity; //identity of the dices' combination
    int value; //the numerical sum of two dices
    //player's choice
    System.out.print(" Enter 1 for random dices, or enter 2 to choose your own dices: ");
    int yourChoice = input.nextInt();//the input from player
    switch ( yourChoice ){
      case 1: value = randomNumber1 + randomNumber2;
        System.out.println(" You picked " + randomNumber1 + " and " + randomNumber2);
        break;
      case 2: 
        System.out.print("Enter the value of the first dice: ");
        int dice1 = input.nextInt(); // input value of the forst dice 
        System.out.print("Enter the value of the second dice: ");
        int dice2 = input.nextInt(); // input value for the second dice
        int ratio = (int) (dice1/7) + (int) (dice2/7); // make sure number !> 6
        switch ( ratio ){ // determine the value of dice is within the range
          case 0: value = dice1 + dice2; 
            break;
          default: value = 0;
            System.out.println("The value of each dice can not be greater than 6.");
            System.exit(1);
            break;
        }
        break;
      default: System.out.print("Enter 1 or 2 only.");
        value = 0;
        System.exit(1);
        break;}
    //assign identity to the dice combination;
    switch ( value%12 ){
      case 2 : identity = " Snake Eyes ";
        break;
      case 3 : identity = " Ace Duece ";
        break;
      case 4 : 
        switch ( randomNumber1/randomNumber2 ){
          case 1: identity = " Hard Four ";
            break;
          default: identity = "Easy Four";
            break;
        }
        break;
      case 5 : identity = " Fever Five ";
        break;
      case 6 : 
        switch ( randomNumber1/randomNumber2 ){
          case 1: identity = " Hard Six ";
            break;
          default: identity = " Easy Six ";
            break;
        }
        break;
      case 7 : identity = " Seven Out ";
        break;
      case 8 : 
        switch ( randomNumber1/randomNumber2 ){
          case 1: identity = " Hard Eight ";
            break;
          default: identity = " Easy Eight ";
            break;
        }
        break;
     case 9 : identity = " Nine ";
        break;
      case 10 :
        switch( randomNumber1/randomNumber2 ){
          case 1: identity = " Hard Ten ";
            break;
          default: identity = " Easy Ten";
            break;
        }
        break;
      case 11: identity = " Yo-leven ";
        break;
      case 0: identity = " Boxcars";
        break;
      default: identity = " ";
        break;
    }
    System.out.println(" You get " + identity);
    System.exit(0);
   }
  }