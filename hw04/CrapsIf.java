//CSE2-SURUI-HW04-if-only
import java.util.*;
public class CrapsIf{
  public static void main(String[] argss){
    Scanner input = new Scanner(System.in);
    //player's choice
    System.out.print("Enter 1 for random dices, or enter 2 for your own choice: ");
    int yourChoice = input.nextInt();
    //generate random number
    int randomNumber1 = (int) (Math.random() * 6 + 1);
    int randomNumber2 = (int) (Math.random() * 6 + 1);
    //identity of the number
    String identityOfDice;
    int numericalValue;
    //selection
   if ( yourChoice == 1){
      numericalValue = randomNumber1 +  randomNumber2;
     System.out.println("Good Luck! You get " + randomNumber1 +" and " + randomNumber2);
    }//randome casting dice
    else if ( yourChoice == 2 ) {
      System.out.print("Enter the number of the first dice: ");
      int playerNumber1 = input.nextInt();
      System.out.print("Enter the number of the second dice: ");
      int playerNumber2 = input.nextInt();
      //assign value to dice
      numericalValue = playerNumber1 + playerNumber2;
      //restrict the input 
      if ( (playerNumber1 > 6) || (playerNumber2 > 6) || (playerNumber1 <= 0) || (playerNumber2 <=0) ){
        System.out.println("The number you enter shoule be between 1 and 6. ");
        System.exit(1);
      }
      else{
        System.out.println("Your choice!");
      }
    }
    else{
      System.out.println("Enter 1 or 2 only.");
      numericalValue = 0;
      System.exit(1);
    }
    //finding identity of dices
    if ( numericalValue == 12 ){
      identityOfDice = " Boxcars ";
    }
    else if ( numericalValue == 2 ){
      identityOfDice = " Snake Eyes ";
    }
    else if ( numericalValue == 3 ){
      identityOfDice = " Ace Duece ";
    }
    else if ( numericalValue == 4 ){
      if ( randomNumber1 == randomNumber2 ){
        identityOfDice = " Hard Four ";
      }
      else {
        identityOfDice = " Easy Four ";
      }
    }
    else if ( numericalValue == 5 ){
      identityOfDice = " Fever Five ";
    }
    else if ( numericalValue == 6 ){
      if ( randomNumber1 == randomNumber2 ){
        identityOfDice = " Hard Six ";
      }
      else {
        identityOfDice = "Easy Six ";
      }
    }
    else if ( numericalValue == 7 ){
      identityOfDice = " Seven Out ";
    }
    else if ( numericalValue == 8 ){
      if ( randomNumber1 == randomNumber2 ){
        identityOfDice = " Hard Eight ";
      }
      else {
        identityOfDice = " Easy Eight ";
      }
    }
    else if ( numericalValue == 9 ){
      identityOfDice = " Nine ";
    }
    else if ( numericalValue == 10 ){
      if ( randomNumber1 == randomNumber2 ){
        identityOfDice = " Hard Ten";
      }
      else{
        identityOfDice = " Easy Ten ";
      }
    }
    else {
      identityOfDice = " Yo-leven";
    }
    //print out results
    System.out.println(" You get " + identityOfDice );
    System.exit(0);
  }
}