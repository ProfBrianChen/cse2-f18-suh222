//CSE2-Surui-lab02-Sep6.
//This program is used for measuring the time, counts in order to calculate the distance and speed
public class Cyclometer {
  public static void main (String [] args){
    //input data//
    int secsTripl = 480; //time of the first trip
    int secsTrip2 = 3220; //time of the second trip
    int countsTrip1 = 1561; //counts of the first trip 
    int countsTrip2 = 9037; //counts of the second trip 
    // intermediate variables and output date
    double wheelDiameter = 27.0, //
    PI = 3.14159, //
    feetPerMile = 5280, //
    inchesPerFoot = 12, //
    secondsPerMinute = 16; //
    double distanceTripl, distanceTrip2, totalDistance; //
    //print out the numbers 
    System.out.println("Trip 1 took " + (secsTripl/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    //run the calculation
    distanceTripl = countsTrip1*wheelDiameter*PI;//distance in inches for trip 1 
    distanceTripl/=inchesPerFoot*feetPerMile; //change inches to miles
    distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;// distance for trip 2 
    totalDistance = distanceTripl + distanceTrip2;
    //print out the distances
    System.out.println("Trip 1 was " + distanceTripl + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance was " + totalDistance + " miles.");
  }
}