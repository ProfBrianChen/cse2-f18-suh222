//CSE2-SURUI-HW06-EncryptedX
import java.util.*;
public class EncryptedX{
  public static void main(String[]args){
    Scanner scnr = new Scanner(System.in);
    System.out.println("Enter an integer between 0 to 100: ");
    //verify the input
    int a = 0;//input integer or not
    int userInput = 0;
    while (a == 0) {
            if (scnr.hasNextInt()) {
                userInput = scnr.nextInt();
                if (userInput > 0 & userInput < 100){
                    a = 1;
                }
                else {
                    System.out.println("Enter again: ");
                }
            }
            else {
                System.out.println("Invalid input, try again: ");
                scnr.next();
            }
        }//end of asking input from the user
        int i = 1;
        int j = 1;
        for (i = 1; i <= userInput + 1; ++i) { //print out userinput + 1 line
            for (j = 1; j <=userInput + 1; ++j){ // print usernum + 1 characters in each line
                if ((j == i) || (j == userInput-i+2)) {
                    System.out.print(" ");
                }//print space in the place of i or usernum+1-i
                else {
                    System.out.print("*");
                }//other place in the line print *
            }
            System.out.println();
            j = 1;
        }
    }
}