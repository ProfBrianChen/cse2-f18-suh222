// DIAMOND: 1-13, CLUB: 14-26, HEARTS: 27-39, SPADES: 40-52;
import java.util.*;
public class Hw05{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //generate cards
        int status = 0; // control the input while loop
        int count = 0;
        int loop = 0;
        int cardCheck = 0;
        double fourOfAKind = 0;
        double threeOfAkind = 0;
        double twoPair = 0;
        double onePair = 0;
        //ask input from the player
        System.out.println(" Enter how many times do you want to run the programs: ");
        while (status == 0) {
            if (scan.hasNextInt()) {
                status = 1;
                count = scan.nextInt();
            } else {
                System.out.println(" You must enter an integer, try again. ");
                scan.next();
            }
        }
        //while loop of run time
        while ( loop<count ){
            int num1 = (int) (Math.random() * 51 + 1);//end of the first card
            int num2 = (int) (Math.random() * 51 + 1);
            while (cardCheck == 0) {
                if (num1 == num2) {
                    num2 = (int) (Math.random() * 51 + 1);
                } else {
                    cardCheck = 1;
                }
            }//end of the second card
            int num3 = (int) (Math.random() * 51 + 1);
            while (cardCheck == 1) {
                if ((num3 == num1) || (num3 == num2)) {
                    num3 = (int) (Math.random() * 51 + 1);
                }
                else {
                    cardCheck = 2;
                }
            }//end of the third card
            int num4 = (int)(Math.random()*51 + 1);
            while ( cardCheck==2 ){
                if ( (num4==num1) || (num4==num2) || (num4==num3)){
                    num4 = (int)(Math.random()*51 + 1);
                }
                else{
                    cardCheck = 3;
                }
            }//end of the forth card
            int num5 = (int)(Math.random()*51 + 1);
            while (cardCheck == 3){
                if ( (num5==num1) || (num5==num2) || (num5==num3) || (num5==num4)){
                    num5 = (int)(Math.random()*51 + 1);
                }
                else{
                    cardCheck = 4;
                }
            }//end of generate the fifth card
            //assign the conditions
            double c1 = num1%13;
            double c2 = num2%13;
            double c3 = num3%13;
            double c4 = num4%13;
            double c5 = num5%13;
            if( c1==c2 ){
                if( c2==c3 ){
                    if( c3==c4 ){
                        fourOfAKind++; // 1234
                    }
                    else if( c3==c5 ){
                        fourOfAKind++;// 1235
                    }
                    else if( c4==c5 ){
                        onePair++; // 45
                        threeOfAkind++; // 123
                    }
                    else{
                        twoPair++; //12 + 23
                    }
                }
                else if( c2==c4 ){
                    if( c4==c5 ){
                        fourOfAKind++; // 1245
                    }
                    else if( c3==c5 ){
                        onePair++; // 35
                        threeOfAkind++; //124
                    }
                    else{
                        twoPair++;//12 + 24
                    }
                }
                else if( c2==c5 ){
                    if ( c3==c4 ){
                        onePair++;//34
                        threeOfAkind++;//125
                    }
                    else{
                        twoPair++;//12+25
                    }
                }
                else if( c3==c4 ){
                    twoPair++;// 12+34
                }
                else if( c3==c5 ){
                    twoPair++;//12 + 35
                }
                else if( c4==c5 ){
                    twoPair++;//12 + 45
                }
                else{
                    onePair++;//12
                }
            }
            if ( c1==c3 ){
                if( c3==c4 ){
                    if( c4==c5 ){
                        fourOfAKind++; // 1345
                    }
                    else{
                        threeOfAkind++; //134
                    }
                }
                else if( c2==c4 ){
                    twoPair++; // 13+24
                }
                else if( c2==c5 ){
                    twoPair++; // 13 + 25
                }
                else if( c3==c5 ){
                    if ( c2==c4 ){
                        onePair++;// 24
                        threeOfAkind++;//135
                    }
                }
                else if( c4==c5 ){
                    twoPair++;//13 + 45
                }
                else{
                    onePair++; //13
                }
            }
            if( c1==c4 ){
                if( c2==c3 ){
                    twoPair++; //14 + 23
                }
                else if( c2==c5 ){
                    twoPair++; // 14 + 25
                }
                else if( c3==c5 ){
                    twoPair++; //14 + 35
                }
                else if( c4==c5){
                    threeOfAkind++; //145
                }
                else{
                    onePair++; //14
                }
            }
            if( c1==c5 ){
                if( c2==c3 ){
                    twoPair++; //15 + 23
                }
                else if( c2==c4 ){
                    twoPair++; // 15 + 24
                }
                else if( c3==c4 ){
                    twoPair++; // 15 + 34
                }
                else{
                    onePair++; //15
                }
            }
            if( c2==c3 ){
                if( c3==c4 ){
                    if( c4==c5 ){
                        fourOfAKind++; // 2345
                    }
                    else{
                        threeOfAkind++; // 234
                    }
                }
                else if( c3==c5 ){
                    threeOfAkind++; // 235
                }
                else if( c4==c5){
                    twoPair++; //23 +45
                }
                else{
                    onePair++; // 23
                }
            }
            if( c2==c4 ){
                if( c3==c5 ){
                    twoPair++; //24 + 35
                }
                else if( c4==c5 ){
                    threeOfAkind++;// 245
                }
                else{
                    onePair++;//24
                }
            }
            if( c2==c5 ){
                if ( c3==c4 ){
                    twoPair++;//25 + 34
                }
                else{
                    onePair++;//25
                }
            }
            if( c3==c4 ){
                if( c4==c5 ){
                    threeOfAkind++; //345
                }
                else{
                    onePair++;//34
                }
            }
            if( c3==c5 ){
                onePair++;//35
            }
            if( c4==c5 ){
                onePair++;//45
            }
            loop ++;
        }
        //print out results
        System.out.println("The number of loops: " + count);
        System.out.println("The probability of Four-of-a-kind: " + String.format("%.3f",(fourOfAKind/count)) );
        System.out.println("The probability of Three-of-a-kind: " + String.format("%.3f",(threeOfAkind/count)) );
        System.out.println("The probability of Two-pair: " + String.format("%.3f",(twoPair/count)));
        System.out.println("The probability of One-pair: " + String.format("%.3f",(onePair/count)));
    }
}