//cse2-lab09-surui
import java.util.*;

public class lab09 {
    public static void main(String[] args) {
        //declare vars
        int[] array0 = {0,1,2,3,4,5,6,7,8,9};
        int[] array1 = copy(array0);
        int[] array2 = copy(array0);
        //Pass array0 to inverter() and print it out with print()
        inverter(array0);
        print(array0);
        System.out.println();
        //Pass array1 to inverter2() and print it out with print().
        inverter2(array1);
        print(array1);
        System.out.println();
        //Pass array2 to inverter2() and assign the output to array3
        // Print out array3 with print()
        int[] array3 = inverter2(array2);
        print(array3);

    }//end of the main method

    /** copy()
     *  accepts an integer array as input
     *  returns an integer array as output.
     */
    public static int[] copy(int[] list) {
        int[] array1 = new int [list.length];
        for (int i = 0; i < list.length; i++) {
            array1[i] = list[i];
        }
        return array1;
    }//end of the method

    /**inverter()
     *  reverses the order
     *  returns void
     */
    public static void inverter(int[] list) {
        for (int i = 0; i < (list.length/2); i++) {
            int temp = list[i];
           list[i] = list[list.length-1-i];
            list[list.length-1-i] = temp;
        }
    }//end of the method

    /**inverter2()
     *  first uses copy()
     *  uses a copy of the code from inverter() to invert the members of the copy
     *  t returns the copy as output.
     */
    public static int[] inverter2(int[] list) {
        int[] array1 = copy(list);
        inverter(array1);
        return array1;
    }//end of the method

    /**print()
     * integer array as input
     * returns nothing.
     *Use a for-loop to print all members of the array
     */
    public static void print(int[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i] + " ");
        }
    }//end of the method
}