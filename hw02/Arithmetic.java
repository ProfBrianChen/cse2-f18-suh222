//CSE2-Surui-hw02-Arithmetic
//to calculate the total cost with tax
public class Arithmetic {
  public static void main (String[] arg) {
    //create variables
    int numPants = 3; //number of pants
    double pantsPrice = 34.98; //price of each pant in 2 decimal
    int numShirts = 2; //number of shirt
    double shirtPrice = 24.99; //price of each shirt in 2 decimal
    int numBelts = 1; //number of belt
    double beltCost = 33.99; //cost of each belt in 2 decimal 
    double paSalesTax = 0.06; //sales tax rate of PA
    // intermediate variables and calculation
    //Total cost of each item
    double totalCostOfPants = numPants*pantsPrice; //total cost of pants 
    double totalCostOfShirts = numShirts*shirtPrice; //total cost of shirts
    double totalCostOfBelts = numBelts*beltCost; //total cost of belts 
    // Sales tax charged to each items
    double pantsSalesTax = totalCostOfPants*paSalesTax;
    double shirtsSalesTax = totalCostOfShirts*paSalesTax;
    double beltSalesTax = totalCostOfBelts*paSalesTax;
    //Total cost of purchase before tax 
    double totalCostOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    //total sales tax
    double totalSalesTax = pantsSalesTax + shirtsSalesTax + beltSalesTax;
    //total paid for the transaction
    double totalPaid = totalCostOfPurchase + totalSalesTax;
    //print out 
    System.out.println("Total cost of pants is " + totalCostOfPants+" dollars.");
    System.out.println("Total cost of shirts is " + totalCostOfShirts + " dollars.");
    System.out.println("Total cost of belts is " + totalCostOfBelts + " dollars.");
    System.out.println("Sales tax of pants is " + String.format("%.2f",pantsSalesTax)+" dollars.");
    System.out.println("Sales tax of shirts is " + String.format("%.2f",shirtsSalesTax) + " dollars.");
    System.out.println("Sales tax of belts is " + String.format("%.2f",beltSalesTax) + " dollars.");
    System.out.println("Total cost of purchase before tax is " + String.format("%.2f",totalCostOfPurchase) + " dollars.");
    System.out.println("Total sales tax is "+ String.format("%.2f",totalSalesTax) + " dollars.");
    System.out.println("Total paid for this transaction is " + String.format("%.2f",totalPaid) +" dollars.");
  }
}