//CSE2-SURUI-HW9-LinearSearch- program1
/**
 * Check that the user only enters ints
 * a different error message for an int that is out of the range from 0-100
 *  a third error message if the int is not greater than or equal to the last int
 *  print final array
 *
 */

import java.util.*;
public class CSE2Linear {
   public static void main (String[] args) {
       Scanner scnr = new Scanner(System.in);
       //prompt user to enter ascending scores
       System.out.println("Enter 15 ascending ints for fina grades in CSE2: ");
       //vars
       int a = 0;//control the int loop
       int[] array1 = new int[15];
       int grade = 0;
       while (a == 0) {
           if (scnr.hasNextInt()) {
               a = 1;
               while (a == 1) {
                   grade = scnr.nextInt();
                   if (grade >= 0 && grade <= 100) {
                       array1[0] = grade;
                       a = 2;
                   } else {
                       System.out.println("Out of range: ");
                   }
               }
           }
           else {
               System.out.println("Int only: ");
               scnr.next();
           }
       }// set the first value of the array
       for (int i = 1; i < 15; i++) {
           a = 2;
           while (a == 2) {
               if (scnr.hasNextInt()) {
                   a = 3;
                   while (a == 3) {
                       grade = scnr.nextInt();
                       if (grade >= 0 && grade <= 100) {
                           if (grade >= array1[i-1]) {
                               a = 4;//exit the loop
                               array1[i] = grade;
                           }
                           else {
                               System.out.println("Must be ascending order: ");
                           }
                       } else {
                           System.out.println("Out of range: ");
                       }
                   }
               }
               else {
                   System.out.println("Int only: ");
                   scnr.next();
               }
           }
       }// end of input grade
       printArray(array1);
       //prompt user to enter grade to search
       System.out.println("Enter a grade to search for: ");
       int search = 0;
       int b = 0;
       while (b == 0) {
           if (scnr.hasNextInt()) {
               search = scnr.nextInt();
               b = 1;
           }
           else {
               System.out.println("key must be int");
           }
       }
       binarySearch(array1, search);
       //scramble and print
       scramble(array1);
       printArray(array1);
       //linear search
       System.out.println("Enter a grade to search for: ");
       while (b == 1) {
           if (scnr.hasNextInt()) {
               search = scnr.nextInt();
               b = 2;
           }
           else {
               System.out.println("key must be int");
           }
       }       linearSearch(array1, search);
   } //end of the main method
    /**
     * print array
     */
    public static void printArray(int[] list) {
        System.out.println("The 15 ascending ints for final grade in CSE2: ");
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i] + " ");
        }
        System.out.println();
    } //end of the printArray
    /**
     * binary search
     */
    public static void binarySearch(int[] list, int key){
       int low = 0;
       int high = list.length - 1;
       int count = 0;
       while (high >= low) {
           count++;
           int mid = (low + high)/2;
           if (key < list[mid]) {
               high = mid - 1; //lower part
           }
           else if (key > list[mid]) {
               low = mid + 1; // higher part
           }
           else {
               System.out.println(key + " was found in the list with " + count + " iterations");
               return; //exit the loop
           }
       }
        System.out.println(key + " was not found in the list with " + count + " iterations");
    } // end of binary search method
    /**
     * scramble
     */
    public static void scramble(int[] list) {
        System.out.println("Scrambled: ");
        for (int i = list.length - 1; i > 0; i--) {
            int j = (int)(Math.random() * (i + 1));
            int temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    } // end of scramble method
    /**
     * linear search
     */
    public static void linearSearch(int[] list, int key) {
        int count = 0;
        for (int i = 0; i < list.length; i++) {
            count++;
            if (key == list[i]) {
                System.out.println(key + " was found in the list with " + count + " iterations.");
                return;
            }
        }
        System.out.println(key + " was not found in the list with " + count + " iterations.");
    }//end of linear search method
}
