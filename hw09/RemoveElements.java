//CSE2-SURUI-HW09-Remove elements

import java.util.*;
public class RemoveElements {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num[] = new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer = "";
        do {
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput(); // SAVE FOR IMPLEMENT
            String out = "The original array is:";
            out += listArray(num); //SAVE FOR IMPLEMENT
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num, index);
            String out1 = "The output array is ";
            out1 += listArray(newArray1); // return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num, target);
            String out2 = "The output array is ";
            out2 += listArray(newArray2); // return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer = scan.next();
        } while (answer.equals("Y") || answer.equals("y"));

    }//end of the main method

    /**
     * listArray method
     */
    public static String listArray(int num[]) {
        String out = "{";
        for (int j = 0; j < num.length; j++) {
            if (j > 0) {
                out += ", ";
            }
            out += num[j];
        }
        out += "} ";
        return out;
    } // end of listArray method

    /**
     * Random input method
     * 10 random integers between 0 to 9
     * returns the filled array
     */
    public static int[] randomInput() {
        int[] list = new int[10];
        for (int i = 0; i < 10; i++) {
            list[i] = (int) (Math.random() * 11);
        }
        return list;
    } // end of random input method

    /**
     * delete(list, pos)
     * return a new array
     * exclude the member in the position pos
     */
    public static int[] delete(int[] list, int pos) {
        if (pos < 0 || pos > list.length - 1) {
            System.out.println("Out of boundary");
            return null;
        }
        int[] list1 = new int[list.length - 1];
        for (int i = 0; i < list1.length; i++) {
            if (i < pos) {
                list1[i] = list[i];
            } else {
                list1[i] = list[i + 1];
            }
        }
        return list1;
    } // end of delete method

    /**
     * remove(list, target)
     * deletes all the elements that are equal to target
     * returning a new list without all those new elements
     */
    public static int[] remove(int[] list, int target) {
        int count = 0;
        for (int a = 0; a < list.length; a++) {
            if (target == list[a]) {
                count++;
            }
        }

        int[] list2 = new int[list.length - count];
        for (int i = 0, j = 0;  i < list.length & j < list2.length ; i++, j++) {
            if (target == list[i]) {
                i++;
            }
            list2[j] = list[i];
        }
        return list2;
    } //end of remove method
}