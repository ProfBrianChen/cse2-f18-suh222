//CSE2-LAB07-SURUI-STORYGENERATOR
import java.util.*;

public class StoryGenerator {
    /**main method*/
    public static void  main(String[] args) {
        //define variables
        int i = 0;
        int j = 0;
        String subject = subject("0");
        Scanner scnr = new Scanner(System.in);
        System.out.println("Would you like some story? (0 for yes/ other numbers for no)");
        while (i == 0){
            if (scnr.hasNextInt()) {
                int userInput = scnr.nextInt();
                if (userInput == 0) {
                    //*THIS IS THESIS
                    //subject = subject("0");//set subject;
                    System.out.println("The " + adjectives("0") + " " + adjectives("1")
                    + " " + subject + " " + verb("0") + " the " + adjectives("2")
                    + " " + object("0") + ".");
                    while (j == 0){
                        System.out.println("More? (0 for yes and other numbers for no)");
                        if (scnr.hasNextInt()) {
                            int userChoice = scnr.nextInt();
                            if (userChoice == 0){
                                System.out.println(paragraph(subject));
                            } else {
                                System.out.println("The end.");
                                j = 1;
                            }
                        } else {
                            System.out.println("Number only");
                            scnr.next();
                        }
                    }
                } else {
                    System.out.println("bye. ");
                }
                i = 1;
            } else {
                System.out.println("Invalid input ");
                scnr.next();
            }
        }
    }
    /**generate adjectives*/
    public static String adjectives(String adj1) {
        Random randomGenerator = new Random();
        int num1 = randomGenerator.nextInt(10); //generate less than 10
        switch (num1) {
            case 0: adj1 = "happy";
            break;
            case 1: adj1 = "mad";
            break;
            case 2: adj1 = "sad";
            break;
            case 3: adj1 = "hungry";
            break;
            case 4: adj1 = "faithful";
            break;
            case 5: adj1 = "awesome";
            break;
            case 6: adj1 = "beautiful";
            break;
            case 7: adj1 = "lovely";
            break;
            case 8: adj1 = "crazy";
            break;
            case 9: adj1 = "dead";
            break;
        }
        return adj1;
    }//end of the adj method
    /**Non-primary nouns appropriate for the subject of a sentence*/
    public static String subject(String subject1) {
        Random randomGenerator = new Random();
        int num2 = randomGenerator.nextInt(10); //generate less than 10
        switch (num2) {
            case 0: subject1 = "mouse";
                break;
            case 1: subject1 = "ox";
                break;
            case 2: subject1 = "tiger";
                break;
            case 3: subject1 = "rabbit";
                break;
            case 4: subject1 = "dragon";
                break;
            case 5: subject1 = "snake";
                break;
            case 6: subject1 = "horse";
                break;
            case 7: subject1 = "sheep";
                break;
            case 8: subject1 = "monkey";
                break;
            case 9: subject1 = "dog";
                break;
        }
        return subject1;
    }//end of the subject method
    /**paste-tense verbs*/
    public static String verb(String verb) {
        Random randomGenerator = new Random();
        int num3 = randomGenerator.nextInt(10); //generate less than 10
        switch (num3) {
            case 0: verb = "ate";
                break;
            case 1: verb = "drank";
                break;
            case 2: verb = "ran";
                break;
            case 3: verb = "caught";
                break;
            case 4: verb = "passed";
                break;
            case 5: verb = "defeated";
                break;
            case 6: verb = "conquered";
                break;
            case 7: verb = "bought";
                break;
            case 8: verb = "wrote";
                break;
            case 9: verb = "cooked";
                break;
        }
        return verb;
    }//end of the verb method
    /**Non-primary nouns appropriate for the object of the sentence.*/
    public static String object(String object) {
        Random randomGenerator = new Random();
        int num4 = randomGenerator.nextInt(10); //generate less than 10
        switch (num4) {
            case 0: object = "apple";
                break;
            case 1: object = "peach";
                break;
            case 2: object = "brownie";
                break;
            case 3: object = "cake";
                break;
            case 4: object = "coffee";
                break;
            case 5: object = "starwar";
                break;
            case 6: object = "pokemon";
                break;
            case 7: object = "digimon";
                break;
            case 8: object = "warship";
                break;
            case 9: object = "computer";
                break;
        }
        return object;
    }//end of the object method
    /**paragraph*/
    public static String paragraph(String p){
        String p1 = "This was "+ adjectives("") + " to " + adjectives("") + " " +object("") + ". ";
        String p2 = "It used " + object("") + " to " + verb("") + " " + object("") + "at the "
                + adjectives("") + " " +object("") + ". ";
        String p3 = "That " + subject("") + " " + verb("") + " her " + object("") + ". ";
        return  p = p1 + p2+ p3;
    }
}