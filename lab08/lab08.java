//CSE2-SUH222-LAB08
import java.util.*;

public class lab08 {
    public static void main(String []args) {
        int num = (int) (Math.random() * 101); //generate random number
        //declaration
        int[] array1 = new int[100];
        int[] array2 = new int[100];
        int count = 0;
        //fill in array 1
        System.out.print("Array 1 holds the following integers: ");
        for (int i = 0; i < 100; i++) {
            num = (int) (Math.random() * 100);
            array1[i] = num;
            System.out.print(array1[i] + " ");
        }//end of loop
        System.out.println("");
        //array2
        for (int j = 0; j < 100; j++) {
            count = 0;
            for (int z = 0; z < 100; z++) {
                if (array1[z] == j) {
                    count++;
                }
            }//end of z loop
            array2[j] = count;
            System.out.println(j + " occurs " + array2[j] + " times");
        }//end of loop
    }
}